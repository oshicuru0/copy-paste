package or.app.copypasteapp.service;

import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import or.app.copypasteapp.Dashboard;
import or.app.copypasteapp.DatabaseHandler;
import or.app.copypasteapp.R;

/**
 * Monitors the {@link ClipboardManager} for changes and logs the text to a file.
 */
public class ClipboardMonitorService extends Service {
    private ClipboardManager mClipboardManager;

    @Override
    public void onCreate() {
        super.onCreate();
        mClipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);

        if (mClipboardManager == null) {
            return;
        }

        mClipboardManager.addPrimaryClipChangedListener(mOnPrimaryClipChangedListener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mClipboardManager != null) {
            mClipboardManager.removePrimaryClipChangedListener(mOnPrimaryClipChangedListener);
        }

        if (mWindowManager != null) {
            mWindowManager.removeViewImmediate(layout);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private ClipboardManager.OnPrimaryClipChangedListener mOnPrimaryClipChangedListener = new ClipboardManager.OnPrimaryClipChangedListener() {
        @Override
        public void onPrimaryClipChanged() {
            ClipData clip = mClipboardManager.getPrimaryClip();
            showView(clip);
        }
    };

    private View layout;
    private WindowManager mWindowManager;

    private void showView(ClipData clip) {
        mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        if (mWindowManager == null) {
            return;
        }

        int LayoutParamFlags = WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
                | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED;

        WindowManager.LayoutParams params  = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PRIORITY_PHONE,
                LayoutParamFlags,
                PixelFormat.TRANSLUCENT);

        layout = createView(clip);

        if (layout == null) {
            return;
        }

        mWindowManager.addView(layout, params);
    }
    private View createView(final ClipData clip) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        if (inflater == null) {
            return null;
        }

        final View layout = inflater.inflate(R.layout.dialog_layout, null);

        ((TextView) layout.findViewById(R.id.content)).setText(clip.getItemAt(0).getText());

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWindowManager.removeViewImmediate(layout);
            }
        });

        ((Button) layout.findViewById(R.id.save)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWindowManager.removeViewImmediate(layout);
                new DatabaseHandler(ClipboardMonitorService.this).addDashboardData(new Dashboard(clip.getItemAt(0).getText().toString()));
            }
        });

        return layout;
    }
}
