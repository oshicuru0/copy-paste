package or.app.copypasteapp;

/**
 * Created by osman on 27/02/2018.
 */

public class Dashboard {
    public String id;
    public String description;

    public Dashboard() {

    }

    public Dashboard(String description) {
        this.description = description;
    }
}
