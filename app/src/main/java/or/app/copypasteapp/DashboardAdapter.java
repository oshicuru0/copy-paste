package or.app.copypasteapp;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import java.util.ArrayList;
import java.util.List;


public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.InboxViewHolder> {

    private List<Dashboard> dashboardList = new ArrayList<>();

    public class InboxViewHolder extends RecyclerView.ViewHolder {

        public View itemView;
        public TextView textView;
        public ImageView imageView;
        public VideoView videoView;

        public InboxViewHolder(View view) {
            super(view);
            textView = view.findViewById(R.id.description);
            imageView = view.findViewById(R.id.image);
            videoView = view.findViewById(R.id.video);
            itemView = view;
        }
    }

    public DashboardAdapter() {}

    public void setDashboardList(List<Dashboard> dashboardList) {
        this.dashboardList = dashboardList;
        notifyDataSetChanged();
    }

    @Override
    public InboxViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item_message, parent, false);
        return new InboxViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final InboxViewHolder holder, final int position) {
        Dashboard dashboard = dashboardList.get(position);
        if (dashboard.description.endsWith(".jpeg")) {
            holder.imageView.setImageURI(Uri.parse(dashboard.description));
            holder.imageView.setVisibility(View.VISIBLE);
            holder.textView.setVisibility(View.GONE);
            holder.videoView.setVisibility(View.GONE);
        } else if (dashboard.description.endsWith(".mp4")) {
            MediaController controller = new MediaController(holder.videoView.getContext());
            controller.setAnchorView(holder.videoView);
            controller.setMediaPlayer(holder.videoView);
            holder.videoView.setMediaController(controller);
            holder.videoView.setVideoURI(Uri.parse(dashboard.description));
            holder.videoView.seekTo(5);
            holder.imageView.setVisibility(View.GONE);
            holder.textView.setVisibility(View.GONE);
            holder.videoView.setVisibility(View.VISIBLE);
        } else {
            holder.textView.setText(dashboard.description);
            holder.imageView.setVisibility(View.GONE);
            holder.textView.setVisibility(View.VISIBLE);
            holder.videoView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return dashboardList.size();
    }

    public interface OnChatItemClick {
        void onItemClick(int position);
    }
}