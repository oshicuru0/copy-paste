package or.app.copypasteapp;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.VideoView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

import or.app.copypasteapp.service.ClipboardMonitorService;

public class Main2Activity extends AppCompatActivity {


    public static int ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE = 5469;
    private WindowManager mWindowManager;
    private DashboardAdapter dashboardAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        RecyclerView recyclerView = findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        dashboardAdapter = new DashboardAdapter();
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(dashboardAdapter);

        Context sharedContext = null;
        try {
            sharedContext = createPackageContext("or.app.copypasteapp", Context.CONTEXT_INCLUDE_CODE);
            DatabaseHandler handler = new DatabaseHandler(sharedContext);
            dashboardAdapter.setDashboardList(handler.getDashboardData());
        } catch (Exception e) {
        }

        mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        int LayoutParamFlags = WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
                | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED;

        WindowManager.LayoutParams params  = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PRIORITY_PHONE,
                LayoutParamFlags,
                PixelFormat.TRANSLUCENT);

        checkPermission();

        isStoragePermissionGranted();

        Intent intent = getIntent();
        Uri imageUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {
            try {
                InputStream inputStream = getContentResolver().openInputStream(imageUri);
                String root = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
                String path = root + "CopyPasteApp/";
                String fullPath = "";
                if (getContentResolver().getType(imageUri).startsWith("image")) {
                    fullPath = path + new Date().getTime() + ".jpeg";
                } else {
                    fullPath = path + new Date().getTime() + ".mp4";
                }
                File file = new File(path);
                file.mkdirs();
                OutputStream outputStream = new FileOutputStream(fullPath);
                copyStream(inputStream, outputStream);
                mWindowManager.addView(createView("file://" + fullPath), params);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        startService(new Intent(this, ClipboardMonitorService.class));
    }

    @Override
    protected void onResume() {
        super.onResume();

        Context sharedContext = null;
        try {
            sharedContext = createPackageContext("or.app.copypasteapp", Context.CONTEXT_INCLUDE_CODE);
            DatabaseHandler handler = new DatabaseHandler(sharedContext);
            dashboardAdapter.setDashboardList(handler.getDashboardData());
        } catch (Exception e) {
        }
    }

    public void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE);
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE) {
            if (!Settings.canDrawOverlays(this)) {
                checkPermission();
            }
        }
    }

    public void copyStream(InputStream is, OutputStream os) {
        final int buffer_size = 4096;
        try {
            byte[] bytes = new byte[buffer_size];
            for (int count = 0, prog = 0; count != -1; ) {
                count = is.read(bytes);
                if (count != -1) {
                    os.write(bytes, 0, count);
                    prog = prog + count;
                }
            }
            os.flush();
            is.close();
            os.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){

        }
    }

    private View createView(final String path) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        if (inflater == null) {
            return null;
        }

        final View layout = inflater.inflate(R.layout.dialog_layout_image, null);
        final VideoView videoView = layout.findViewById(R.id.video);

        if (path.endsWith(".mp4")) {
            videoView.setVideoURI(Uri.parse(path));
            videoView.seekTo(5);
            ((VideoView) layout.findViewById(R.id.video)).setVisibility(View.VISIBLE);
            ((ImageView) layout.findViewById(R.id.content)).setVisibility(View.GONE);
        } else {
            ((ImageView) layout.findViewById(R.id.content)).setImageURI(Uri.parse(path));
            ((VideoView) layout.findViewById(R.id.video)).setVisibility(View.GONE);
            ((ImageView) layout.findViewById(R.id.content)).setVisibility(View.VISIBLE);
        }

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWindowManager.removeViewImmediate(layout);
            }
        });

        ((Button) layout.findViewById(R.id.save)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoView.invalidate();
                mWindowManager.removeViewImmediate(layout);
                new DatabaseHandler(Main2Activity.this).addDashboardData(new Dashboard(path));
                Context sharedContext = null;
                try {
                    sharedContext = createPackageContext("or.app.copypasteapp", Context.CONTEXT_INCLUDE_CODE);
                    DatabaseHandler handler = new DatabaseHandler(sharedContext);
                    dashboardAdapter.setDashboardList(handler.getDashboardData());
                } catch (Exception e) {
                }
            }
        });

        return layout;
    }
}
